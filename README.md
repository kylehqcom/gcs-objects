# GCS Objects

A Golang util to get Google Cloud Storage (GCS) objects. The majority of this code was cribbed from 
[https://github.com/GoogleCloudPlatform/golang-samples/tree/master/storage/objects](https://github.com/GoogleCloudPlatform/golang-samples/tree/master/storage/objects)

The reason for this util came about due to needing a way to get private files/data
into a CI/CD workflow. As much as I appreciate the 12 factor app principles, having
every single configuration value in a ENV var was just too much overhead.

Instead I have opted to use ENV vars for private key details to communicate with GCS,
return temporary file details, use in the build process and then delete as required.

## Usage

```
usage:

$GC_PRIVATE_KEY env variable must be set
$GC_ACCESS_ID env variable must be set. Normally takes the form of xxx@developer.gserviceaccount.com

-p=google-project-id
-o=bucket:name
-d=output-destination<OPTIONAL>
```

### `GC_PRIVATE_KEY` Env Variable

To obtain your Google Cloud Service Account refer to
[https://cloud.google.com/storage/docs/authentication#service_accounts](https://cloud.google.com/storage/docs/authentication#service_accounts)

Create a key with `PKCS12` format. Then you can `openssl pkcs12 -in key.p12 -passin pass:notasecret -out your-private-key.pem -nodes` to
format into a PEM key. The value of which should be a properly structured RSA key like

```
-----BEGIN RSA PRIVATE KEY-----
123456789012345678901bLK8IJvjXjtaqj6aDjdu3ESt3ssRgmusxcv9IHKfxpS
4BouLKGuXUNt9c6ULQHA1234567890123456789012345678901234567891/fIR
wv9yAdZdsSFjjmy0SSRcB......
```

### `GC_ACCESS_ID` Env Variable

Also known as a *Service Account ID*, you can find this value [https://console.cloud.google.com/iam-admin/serviceaccounts](https://console.cloud.google.com/iam-admin/serviceaccounts) - takes the form of `xxxxx@developer.gserviceaccount.com`

### Example Usage

```
go run main.go -p $GC_PROJECT_ID -o $GCS_BUCKET:$GCS_OBJECT
```

In this example **all** of the env var names are generic and you can replace 
these with local values. It's entirely up to you. Also due to the omission of
the `-d destination` option, the `object` value will be used as the default
output. In this case the value of `$GCS_OBJECT`.

`$GC_PROJECT_ID` is located from [https://console.cloud.google.com/home/dashboard](https://console.cloud.google.com/home/dashboard) and
is the ID **you** assigned to your project.

`$GCS_BUCKET:$GCS_OBJECT` is a tuple of your bucket_name:file_object_name, eg `etc:config.yml`


Pass the `-d destination` option to output the object contents to a folder/name of your
choice. The option should be fully qualified eg `-d ./your/nested/custom-output.yml`
