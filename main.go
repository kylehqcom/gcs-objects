// This package was built with the fabulous examples from https://github.com/rakyll
// https://github.com/GoogleCloudPlatform/golang-samples/tree/master/storage/objects
//
// Due to processing auth with pem file keys, we overwrite the default http.Client to stop
// any default OAuth flow.
package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"golang.org/x/net/context"

	"cloud.google.com/go/storage"
	"google.golang.org/api/option"
)

const helptext = `usage:

$GC_PRIVATE_KEY env variable must be set
$GC_ACCESS_ID env variable must be set. Normally takes the form of xxx@developer.gserviceaccount.com

-p=google-project-id
-o=bucket:name
-d=output-destination<OPTIONAL>
`

func usage(msg string) {
	if msg != "" {
		fmt.Fprintln(os.Stderr, msg)
	}
	fmt.Fprintln(os.Stderr, helptext)
	os.Exit(2)
}

func main() {
	privateKey := os.Getenv("GC_PRIVATE_KEY")
	if privateKey == "" {
		fmt.Fprintf(os.Stderr, "GC_PRIVATE_KEY environment variable must be set.\n")
		os.Exit(1)
	}

	accessID := os.Getenv("GC_ACCESS_ID")
	if accessID == "" {
		fmt.Fprintf(os.Stderr, "GC_ACCESS_ID environment variable must be set.\n")
		os.Exit(1)
	}

	var d, o, p string
	flag.StringVar(&d, "d", "", "output destination; defaults to current directory")
	flag.StringVar(&o, "o", "", "source object; in the format of <bucket:object>")
	flag.StringVar(&p, "p", "", "project id; as listed via console.cloud.google.com")
	flag.Parse()

	if p == "" {
		usage("missing -p flag")
	}

	names := strings.Split(o, ":")
	if len(names) < 2 {
		usage("missing -o flag. Note the : to split bucket-name:object-name")
	}
	bucket, object := names[0], names[1]
	log.Printf("Looking up bucket: %s object: %s", bucket, object)

	// Note the option.WithHTTPClient(http.DefaultClient) which will short circuit the
	// storage.NewClient call and not attempt to apply default OAuth requirements. As
	// we are using a pem key to authorise the requests no OAUth is required. Omitting
	// the option will give the following error.
	//
	// dialing: google.DefaultTokenSource: google: could not find default credentials.
	// See https://developers.google.com/accounts/docs/application-default-credentials for more information.
	ctx := context.Background()
	client, err := storage.NewClient(ctx, option.WithHTTPClient(http.DefaultClient))
	if err != nil {
		usage(err.Error())
	}

	url, err := signedURL(client, bucket, object, accessID, privateKey)
	if err != nil {
		usage(err.Error())
	}
	log.Printf("Successfully return a signed url.")

	b, err := objectContents(url)
	if err != nil {
		usage(err.Error())
	}
	log.Printf("Object contents returned from signed url.")

	// Default the output to that of the object name
	out := object
	if d != "" {
		out = d
	}
	err = persistContents(b, out)
	if err != nil {
		usage(err.Error())
	}
	log.Printf(fmt.Sprintf("Object available at %s", out))
}

// signedURL will attempt to create a signed url for a private bucket:object in Google Cloud storage.
// Download a p12 service account private key from the Google Developers Console.
// And convert it to PEM by running the command below:
// $ openssl pkcs12 -in key.p12 -passin pass:notasecret -out my-private-key.pem -nodes
func signedURL(client *storage.Client, bucket, object, accessID, privateKey string) (string, error) {
	return storage.SignedURL(bucket, object, &storage.SignedURLOptions{
		GoogleAccessID: accessID,
		PrivateKey:     []byte(privateKey),
		Method:         "GET",
		Expires:        time.Now().Add(1 * time.Minute), // 1 minute should be plenty!
	})
}

// objectContents will return the object contents from the signed url.
func objectContents(url string) ([]byte, error) {
	resp, err := http.DefaultClient.Get(url)
	if err != nil {
		return []byte{}, err
	}
	defer resp.Body.Close()
	return ioutil.ReadAll(resp.Body)
}

// persistContents will persist the object contents to the filesystem.
func persistContents(b []byte, output string) error {
	return ioutil.WriteFile(output, b, 0644)
}
